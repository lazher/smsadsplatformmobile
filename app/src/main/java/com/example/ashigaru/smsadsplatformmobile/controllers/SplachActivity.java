package com.example.ashigaru.smsadsplatformmobile.controllers;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.controllers.LoginSignUp.LoginSignupActivity;

public class SplachActivity extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splach);
       // getSupportActionBar().hide();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // Start login activity
                Intent i = new Intent(SplachActivity.this, LoginSignupActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}

