package com.example.ashigaru.smsadsplatformmobile.controllers.LoginSignUp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.ashigaru.smsadsplatformmobile.Base.BaseActivity;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.controllers.Guide.GuideActivity;
import com.example.ashigaru.smsadsplatformmobile.controllers.HomeActivity;


public class LoginSignupActivity extends BaseActivity {


    private static final String SHARED_NAME = "user_prefs";


    @Override
    public int getLayoutId() {
        return R.layout.activity_login ;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_NAME, MODE_PRIVATE);
        Log.v("pref",""+sharedPreferences.getString("Username","").toString());
        if(sharedPreferences.getString("Username","") == "") {
            getSupportFragmentManager().beginTransaction().replace(R.id.logsign_container , new LoginFragment(),"LoginFragment").commit();
        } else {
            finish();
           // startActivity(new Intent(this, HomeActivity.class));
            startActivity(new Intent(this, GuideActivity.class));
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 101) {
            LoginFragment frag = (LoginFragment) getSupportFragmentManager().findFragmentByTag("LoginFragment");
            if (frag != null) {
                frag.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }
}
