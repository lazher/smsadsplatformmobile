package com.example.ashigaru.smsadsplatformmobile.controllers.Guide;

public interface CurrentFragmentListener {
    void currentFragmentPosition(int position);
}
