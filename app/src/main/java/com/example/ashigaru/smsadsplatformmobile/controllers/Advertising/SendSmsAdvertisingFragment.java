package com.example.ashigaru.smsadsplatformmobile.controllers.Advertising;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bitly.Bitly;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bitly.Error;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseFragment;
import com.example.ashigaru.smsadsplatformmobile.Models.Advertising;
import com.example.ashigaru.smsadsplatformmobile.Models.Contact;
import com.example.ashigaru.smsadsplatformmobile.Models.Group;
import com.example.ashigaru.smsadsplatformmobile.Models.Company_contact;
import com.example.ashigaru.smsadsplatformmobile.Models.User;
import com.example.ashigaru.smsadsplatformmobile.Models.Users_ads;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;
import com.example.ashigaru.smsadsplatformmobile.controllers.Dashboard.DashboardFragment;
import com.google.gson.Gson;

import android.telephony.SmsManager;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

import static android.content.Context.MODE_PRIVATE;


public class SendSmsAdvertisingFragment extends BaseFragment implements View.OnClickListener {
    List<Group> hobbiesList;
    List<Company_contact> CompanyContactList;
    @BindView(R.id.sendSms)
    Button sendSms;
    @BindView(R.id.GroupSend)
    Spinner GroupSend;
    @BindView(R.id.edit_text)
    EditText pub;
    final int SEND_SMS_PERMISSION_REQUEST_CODE = 1;
    ProgressDialog progressDialog;
    private ProgressDialog progress;
    ArrayList<String> titreCompanyArray;
    private static final String SHARED_NAME = "user_prefs";
    SharedPreferences shared;
    User current;
    Users_ads ua;
    String  link1 = "";
  int nombreclicks =0 ;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        current = getCurrentUser();

        Bundle bundle = getArguments();
        final Advertising obj = (Advertising) bundle.getSerializable("Advertising");
        Log.v("obj from f2", obj.getPubText() + " " + obj.getDate()+" "+obj.getUrl());

        Bitly.initialize(getContext(), "2d6f0c4d8215f5dfcb24714272a27ade3465a68d");
        Bitly.shorten(obj.getUrl()+"#" + current.getId(), new Bitly.Callback() {

            @Override
            public void onResponse(com.bitly.Response response) {
                // response provides a Response object which contains the shortened Bitlink
                // response includes a status code
                // Your custom logic goes here...
                Log.v("response", " " + response.getBitlink());

                pub.setText(obj.getPubText().toString()+" "+response.getBitlink());
                saveShortlink(response.getBitlink(), obj);
                getClicks(current.getId(), response.getBitlink());
            }

            @Override
            public void onError(Error error) {
                // error provides any errors in shortening the link
                // Your custom logic goes here...
                Log.v("response error", " " + error);
            }
        });

        pub.setText(obj.getPubText().toString()+" "+link1.toString());
        progressDialog = new ProgressDialog(getContext());
        loadHobbies();

       // String link = "http://bit.ly/2Lm31vo";
        // saveShortlink(link, obj);



        Log.v("ng",""+nombreclicks);
       // UpdatenbClick(nombreclicks ,ua );
        sendSms.setEnabled(false);
        if (checkPermission(Manifest.permission.SEND_SMS)) {
            sendSms.setEnabled(true);
            sendSms.setOnClickListener(this);
        } else {
            ActivityCompat.requestPermissions(this.getActivity(),
                    new String[]{Manifest.permission.SEND_SMS}, SEND_SMS_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_send_smsadvertinsing_fragment;
    }

    @Override
    public void onBackPressed() {
        DashboardFragment dashboardFragment = new DashboardFragment();
        getFragmentManager().beginTransaction().replace(R.id.frame_container, dashboardFragment).commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendSms: {
                int id = -1;
                for (int i = 0; i < hobbiesList.size(); i++)
                    if (hobbiesList.get(i).getTitre().equals(GroupSend.getSelectedItem().toString())) {
                        id = hobbiesList.get(i).getId();
                    }
                loadContactCompany(current.getId(), id);

                String company = GroupSend.getSelectedItem().toString();
                Log.v("company", "" + company);

                break;
            }


        }
    }


    public boolean checkPermission(String permission) {
        int check = ContextCompat.checkSelfPermission(this.getActivity(), permission);
        return (check == PackageManager.PERMISSION_GRANTED);
    }

    void loadHobbies() {
        hobbiesList = new ArrayList<Group>();
        titreCompanyArray = new ArrayList<String>();
//        hobbiesList.clear();
        progressDialog.setMessage("Loading company...");
        progressDialog.show();
        String url = Utils.urlServer + "/company/all";
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            Group comp = new Group();
                            try {
                                comp.setId(response.getJSONObject(i).getInt("id"));
                                comp.setIdUser(response.getJSONObject(i).getInt("idUser"));
                                comp.setTitre(response.getJSONObject(i).getString("titre"));
                                comp.setColorRes(R.string.sportstring);
                                comp.setImgResource(R.drawable.friends2);
                                comp.setType("SPORT");
                                titreCompanyArray.add(comp.getTitre());
                                hobbiesList.add(comp);
                                Log.v("size1", "" + String.valueOf(hobbiesList.size()));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.v("ResponseServer", response.toString());
                        progressDialog.dismiss();

                        ArrayAdapter adapter = new ArrayAdapter(getContext(),
                                android.R.layout.simple_spinner_item, titreCompanyArray);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        GroupSend.setAdapter(adapter);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Please Check your Internet Connection!", Toast.LENGTH_SHORT).show();

                        Log.v("ResponseServer", error.toString());


                    }
                }
        );

        // Add JsonObjectRequest to the RequestQueue
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

    }

    void loadContactCompany(int idUser, int idCompany) {
        CompanyContactList = new ArrayList<Company_contact>();
        titreCompanyArray = new ArrayList<String>();
//        hobbiesList.clear();


        String url = Utils.urlServer + "/company_contact/alll/" + idUser + "/" + idCompany;
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            Company_contact comp = new Company_contact();
                            try {
                                comp.setId(response.getJSONObject(i).getInt("id"));
                                comp.setId_user(response.getJSONObject(i).getInt("id_user"));
                                comp.setId_company(response.getJSONObject(i).getInt("id_company"));
                                comp.setId_contact(BigInteger.valueOf(response.getJSONObject(i).getLong("id_contact")));

                                CompanyContactList.add(comp);
                                Log.v("sizeCompanyContactList", "" + String.valueOf(CompanyContactList.size()));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.v("ResponseServer", response.toString());
                        progressDialog.dismiss();
                        sendSms();

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Please Check your Internet Connection!", Toast.LENGTH_SHORT).show();

                        Log.v("ResponseServer", error.toString());


                    }
                }
        );

        // Add JsonObjectRequest to the RequestQueue
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);


    }

    void sendSms() {
        String smsMessage = pub.getText().toString();

        try {
            SmsManager smgr = SmsManager.getDefault();
            for (int i = 0; i < CompanyContactList.size(); i++) {
                String phonenum = "" + CompanyContactList.get(i).getId_contact();
                Log.v("num", CompanyContactList.get(i).toString());

                smgr.sendTextMessage(phonenum, null, smsMessage, null, null);

            }
            //   smgr.sendTextMessage(phoneNumber ,null,smsMessage,null,null);
            Toast.makeText(this.getContext(), "SMS Sent Successfully", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this.getContext(), "SMS Failed to Send, Please try again", Toast.LENGTH_SHORT).show();
        }
    }


    private User getCurrentUser() {
        Gson gson = new Gson();
        shared = getActivity().getSharedPreferences(SHARED_NAME, MODE_PRIVATE);
        String jsonCurrent_user = shared.getString("current_user", "");
        User user = gson.fromJson(jsonCurrent_user, User.class);
        return user;
    }


    public void getClicks(int idUser, String link) {

        String Adresse = "https://api-ssl.bitly.com/v3/clicks?access_token=2d6f0c4d8215f5dfcb24714272a27ade3465a68d";
//        hobbiesList.clear();


        String url = Adresse + "&shortUrl=" + link;
        Log.v("linklonk", "" + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                        //  Log.v("nb",""+response.toString());
                        try {
                            JSONObject nb = response.getJSONObject("data");
                            JSONObject object = new JSONObject(nb.toString());
                            JSONArray Jarray = object.getJSONArray("clicks");

                            for (int i = 0; i < Jarray.length(); i++) {
                                JSONObject Jasonobject = Jarray.getJSONObject(i);
                                 nombreclicks = Jasonobject.getInt("user_clicks");
                                Log.v("nombreclicks", " " + nombreclicks );
                                UpdatenbClick(nombreclicks,ua);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.v("ResponseServer", response.toString());
                        progressDialog.dismiss();


                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Please Check your Internet Connection!", Toast.LENGTH_SHORT).show();

                        Log.v("ResponseServer", error.toString());


                    }
                }
        );

        // Add JsonObjectRequest to the RequestQueue
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

    }


    private void saveShortlink(final String shortLink, final Advertising ad) {

        String url = Utils.urlServer + "/users_pubs/new";
        //Utils.showProgressDialog(progress, "SmsAdsPlatform", "Adding Contact", false);

        StringRequest rq = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Response Error", error.toString());
                Toast.makeText(getContext(), "error", Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tag", "new");
                params.put("id_user", "" + current.getId());
                params.put("shortlink", shortLink.toString());
                params.put("id_pubs", "" + ad.getId());


                return params;
            }

        };


        VolleySingleton.getInstance(getContext()).addToRequestQueue(rq);
    }


    private void UpdatenbClick(final int nbc, Users_ads ua) {

        progressDialog.show();

        String url = Utils.urlServer + "/users_pubs/update/" + ua.getId();
        Utils.showProgressDialog(progressDialog, "SmsAdsPlatform", "Update Contacts..", false);
        StringRequest jsonObjectRequest = new StringRequest(
                Request.Method.PUT,
                url,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("ResponseServer", response.toString());


                        if (response.isEmpty()) {
                            progressDialog.dismiss();
                            //Utils.showAlert(getActivity(), "Update Contacts", "Failed to Update! ");
                        } else {
                            progressDialog.dismiss();
                            //Utils.showAlert(getActivity(), "Update Contacts", "Success to Opdate!");

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Please Check your Internet Connection!", Toast.LENGTH_SHORT).show();
                        // Do something when error occurred

                        Log.v("ResponseServer", error.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {


                Map<String, String> params = new HashMap<String, String>();

                params.put("tag", "new");

                params.put("nbclick", "" + nbc);

                return params;
            }

        };

        VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

    }

}