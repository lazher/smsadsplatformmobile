package com.example.ashigaru.smsadsplatformmobile.controllers.Contacts;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ashigaru.smsadsplatformmobile.Base.BaseActivity;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseFragment;
import com.example.ashigaru.smsadsplatformmobile.Models.Contact;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.controllers.Dashboard.DashboardFragment;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ContactFragment extends BaseFragment {
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    //List<Contact> propositionList;
    public String android_contact_Name = "";
    public String android_contact_TelefonNr = "";
    public int android_contact_ID=0;
    List<Contact> arrayList_Android_Contacts;
    @BindView(R.id.image_empty)
    ImageView emptyImg;
    Contact contact;
    ContactAdapter adapter;
    LinearLayoutManager mLayoutManager;
    List<Contact> contactsList;
    @BindView(R.id.listInvitation)
    RecyclerView listInvitation;
    ProgressDialog progressDialog;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((BaseActivity) getActivity()).setOnBackPressedListener(this);
        contactsList = new ArrayList<>();
        progressDialog = new ProgressDialog(getContext());progressDialog.setMessage("... reloading");
       getALL_Android_Contacts();
        showContacts(arrayList_Android_Contacts);
        // getALL_Android_Contacts();

        progressDialog = new ProgressDialog(getContext());progressDialog.setMessage("... reloading");
        progressDialog.show();
        super.setTitle("Contacts");
        progressDialog.dismiss();

    }
    public void showContacts(List<Contact> l) {


        mLayoutManager = new LinearLayoutManager(getActivity());
        listInvitation.setLayoutManager(mLayoutManager);
        Log.v("HERE", "HERE");

        adapter = new ContactAdapter(getActivity(), l);
        listInvitation.setAdapter(adapter);

    }
    @Override
    public int getLayoutId() {
        return R.layout.activity_contact_fragment;


    }

    @Override
    public void onBackPressed() {
        DashboardFragment dashboardFragment = new DashboardFragment();
        getFragmentManager().beginTransaction().replace(R.id.frame_container, dashboardFragment).commit();
    }


    public void getALL_Android_Contacts() {

       // if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
       //    Log.v("yes","good ");}
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ActivityCompat.checkSelfPermission(getContext().getApplicationContext(),Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
       arrayList_Android_Contacts = new ArrayList<Contact>();

    //get all Contacts
        Cursor cursor_Android_Contacts = null;
        ContentResolver contentResolver =  getActivity().getContentResolver();
        try {
            cursor_Android_Contacts = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        } catch (Exception ex) {
            Log.e("Error on contact", ex.getMessage());
        }

        if(cursor_Android_Contacts==null)
        {Log.v( "vide","vide");
         }
            // Check: hasContacts
        if (cursor_Android_Contacts.getCount() > 0) {
            emptyImg.setVisibility(View.GONE);
            //has Contacts
            //  @Loop: all Contacts
            while (cursor_Android_Contacts.moveToNext()) {

                Contact android_contact = new Contact();
                String contact_id = cursor_Android_Contacts.getString(cursor_Android_Contacts.getColumnIndex(ContactsContract.Contacts._ID));
                String contact_display_name = cursor_Android_Contacts.getString(cursor_Android_Contacts.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));


                //set
                android_contact.setFullName(contact_display_name.toString());


                // get phone number
                int hasPhoneNumber = Integer.parseInt(cursor_Android_Contacts.getString(cursor_Android_Contacts.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {

                    Cursor phoneCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI
                            , null
                            , ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?"
                            , new String[]{contact_id}
                            , null);

                    while (phoneCursor.moveToNext()) {
                        String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//< set >
                        Log.v("number","number : "+phoneNumber);



                        try {

                            android_contact.setNumTel(phoneNumber);

                        }catch (NumberFormatException e){
                            System.out.println("not a number");
                        }

//</ set >
                    }
                    phoneCursor.close();
                }
//----</ set >----
//----</ get phone number >----

// Add the contact to the ArrayList
                Log.v("Contacts", "contact : " + android_contact.getFullName() );
                arrayList_Android_Contacts.add(android_contact);

            }
//----</ @Loop: all Contacts >----

//< show results >

//</ show results >
        }

        }         }





    public static int strToInt( String str ){
        int i = 0;
        int num = 0;
        boolean isNeg = false;

        //Check for negative sign; if it's there, set the isNeg flag
        if (str.charAt(0) == '-') {
            isNeg = true;
            i = 1;
        }

        //Process each character of the string;
        while( i < str.length()) {
            num *= 10;
            num += str.charAt(i++) - '0'; //Minus the ASCII code of '0' to get the value of the charAt(i++).
        }

        if (isNeg)
            num = -num;
        return num;
    }




}
