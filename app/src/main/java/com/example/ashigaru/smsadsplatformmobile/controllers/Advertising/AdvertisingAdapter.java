package com.example.ashigaru.smsadsplatformmobile.controllers.Advertising;

import android.app.ProgressDialog;
import android.content.Context;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.example.ashigaru.smsadsplatformmobile.Models.Advertising;

import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdvertisingAdapter extends RecyclerSwipeAdapter<AdvertisingAdapter.ViewHolder> {

    List<Advertising> listAdvertising;
    View rowView;
    private Context mContext;

    public AdvertisingAdapter(Context mContext, List<Advertising> listAdvertising) {
        this.listAdvertising = listAdvertising;
        this.mContext = mContext;
    }

    @Override
    public AdvertisingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_advertising, parent, false);
        AdvertisingAdapter.ViewHolder viewHolder = new AdvertisingAdapter.ViewHolder(rowView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AdvertisingAdapter.ViewHolder viewHolder, int position) {
        Advertising ad = listAdvertising.get(position);
        // Date date = null;
        // SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        // try {
        //    date = format.parse(proposition.getDate());
        //System.out.println(date);
        //} catch (ParseException e) {
        //    e.printStackTrace();
        //}
        //Calendar cal = Calendar.getInstance();
        //cal.setTime(date);
        //int year = cal.get(Calendar.YEAR);
        // int month = cal.get(Calendar.MONTH);
        //int day = cal.get(Calendar.DAY_OF_MONTH);
        //System.out.println(date);
        //System.out.println(year+"-"+month+"-"+day);;
        viewHolder.dateInvit.setText(ad.getDate().toString());
        viewHolder.subjectInvit.setText(ad.getPubText());
        viewHolder.userName.setText(""+ad.getUrl());
        viewHolder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {

            @Override
            public void onOpen(SwipeLayout layout) {
                mItemManger.closeAllExcept(layout);
            }

        });

        mItemManger.bindView(viewHolder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return (null != listAdvertising ? listAdvertising.size() : 0);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeInvitation;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout linearLayoutRefuse, linearLayoutAccept,swipeElememnt;
        SwipeLayout swipeLayout;
        TextView timeInvit;
        TextView dateInvit;
        TextView subjectInvit;
        TextView userName;
        CircleImageView imgUser;

        public ViewHolder(View view) {
            super(view);
            this.swipeLayout =  view.findViewById(R.id.swipeInvitation);
            this.timeInvit =  view.findViewById(R.id.timeInvit);
            this.dateInvit = view.findViewById(R.id.dateInvit);
            this.subjectInvit =  view.findViewById(R.id.subjectInvit);
            this.userName =  view.findViewById(R.id.userName);
            this.imgUser =  view.findViewById(R.id.imgUser);
            this.swipeElememnt = view.findViewById(R.id.swipeElememnt);
            this.linearLayoutRefuse =  view.findViewById(R.id.linearLayoutRefuse);
            this.linearLayoutAccept =  view.findViewById(R.id.linearLayoutAccept);
            linearLayoutAccept.setOnClickListener(this);
            linearLayoutRefuse.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.linearLayoutRefuse: {

                    swipeLayout.close();
                    break;
                }
                case R.id.linearLayoutAccept: {

                    swipeLayout.close();


                    SendSmsAdvertisingFragment sendSmsAdvertisingFragment = new SendSmsAdvertisingFragment();
                    Advertising adsSelect = new Advertising();
                    adsSelect.setId(listAdvertising.get(getAdapterPosition()).getId());
                    adsSelect.setDate(listAdvertising.get(getAdapterPosition()).getDate());
                    adsSelect.setPubText(listAdvertising.get(getAdapterPosition()).getPubText());
                    adsSelect.setUrl(listAdvertising.get(getAdapterPosition()).getUrl());
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Advertising",adsSelect);
                    sendSmsAdvertisingFragment.setArguments(bundle);
                    Log.v("selectAds",""+listAdvertising.get(getAdapterPosition()).getDate()+" "+listAdvertising.get(getAdapterPosition()).getPubText());
                    ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, sendSmsAdvertisingFragment).commit();



                    break;
                }
            }
        }

        private void acceptInvitation(final Advertising id) {
            final ProgressDialog progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Accepting...");
            progressDialog.show();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.GET,
                    Utils.urlServer +"/propositionService/acceptProposition?idInvit=",
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            progressDialog.dismiss();
                            Log.v("ResponseServer" , response.toString());
                            Toast.makeText(mContext,"Invitation was Accepted", Toast.LENGTH_LONG).show();
                            listAdvertising.remove(id);
                            notifyDataSetChanged();
                        }
                    },
                    new Response.ErrorListener(){
                        @Override
                        public void onErrorResponse(VolleyError error){
                            progressDialog.dismiss();
                            Toast.makeText(mContext,"Please Check your Internet Connection!" , Toast.LENGTH_SHORT).show();
                            Log.v("ResponseServer" , error.toString());

                        }
                    }
            );
            // Add JsonObjectRequest to the RequestQueue
            VolleySingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
        }

        private void refuseInvitation(final Advertising id) {
            final ProgressDialog progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Accepting...");
            progressDialog.show();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.GET,
                    Utils.urlServer +"/propositionService/refuseProposition?idInvit=",
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            progressDialog.dismiss();
                            Toast.makeText(mContext,"Invitation was Refused", Toast.LENGTH_LONG).show();
                            listAdvertising.remove(id);
                            notifyDataSetChanged();
                        }
                    },
                    new Response.ErrorListener(){
                        @Override
                        public void onErrorResponse(VolleyError error){
                            progressDialog.dismiss();
                            Toast.makeText(mContext,"Please Check your Internet Connection!" , Toast.LENGTH_SHORT).show();
                            Log.v("ResponseServer" , error.toString());
                        }
                    }
            );
            // Add JsonObjectRequest to the RequestQueue
            VolleySingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
        }
    }



    public void updateData(final List<Advertising> propositions ) {
        listAdvertising= new ArrayList<>();
        listAdvertising.addAll(propositions);
    }
}

