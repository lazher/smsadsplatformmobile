package com.example.ashigaru.smsadsplatformmobile;

import android.app.Activity;
import android.app.ProgressDialog;

import com.example.ashigaru.smsadsplatformmobile.controllers.Groups.AlertDialogAddCompany;
import com.example.ashigaru.smsadsplatformmobile.controllers.Contacts.AlertDialogListCompany;

public class Utils {

    /**
     * Show ProgressDialog in View
     *
     * @param progress
     * @param title
     * @param message
     * @param cancelable
     */
    public static void showProgressDialog(ProgressDialog progress, String title, String message, boolean cancelable) {
        progress.setTitle(title);
        progress.setMessage(message);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(cancelable);
        progress.show();
    }

    public static String urlServer = "http://192.168.1.5:3000" ;
    public static String ipServer = "192.168.1.3" ;

    public static void showAlert(Activity context, String title, String message) {
        AlertDialogFragment.newInstance(title, message).showAlert(context);
    }

    public static void showDialogHobbiesAlert(Activity context) {
        AlertDialogAddCompany.newInstance().showAlert(context);
    }

    public static void showDialogCompanyAlert(Activity context) {
        AlertDialogListCompany.newInstance().showAlert(context);

    }

}
