package com.example.ashigaru.smsadsplatformmobile.Models;

public class Users_ads {

    private int id;
    private int id_user;
    private int id_pubs;
    private String shortlink;
    private int nbclick;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_pubs() {
        return id_pubs;
    }

    public void setId_pubs(int id_pubs) {
        this.id_pubs = id_pubs;
    }

    public String getShortlink() {
        return shortlink;
    }

    public void setShortlink(String shortlink) {
        this.shortlink = shortlink;
    }

    public int getNbclick() {
        return nbclick;
    }

    public void setNbclick(int nbclick) {
        this.nbclick = nbclick;
    }
    public Users_ads(){}
    public Users_ads(int id, int id_user, int id_pubs, String shortlink, int nbclick) {
        this.id = id;
        this.id_user = id_user;
        this.id_pubs = id_pubs;
        this.shortlink = shortlink;
        this.nbclick = nbclick;
    }
}
