package com.example.ashigaru.smsadsplatformmobile.controllers.Contacts;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseActivity;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseFragment;
import com.example.ashigaru.smsadsplatformmobile.Models.Contact;
import com.example.ashigaru.smsadsplatformmobile.Models.User;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;
import com.example.ashigaru.smsadsplatformmobile.controllers.Dashboard.DashboardFragment;
import com.example.ashigaru.smsadsplatformmobile.controllers.Invitation.InvitationRVAdapter;
import com.example.ashigaru.smsadsplatformmobile.controllers.Invitation.RecyclerItemClickListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

import static android.content.Context.MODE_PRIVATE;

public class ContactsSavedFragment extends BaseFragment {
    //List<Contact> propositionList;
    boolean isMultiSelect = false;

    private static final String SHARED_NAME = "user_prefs";
    SharedPreferences shared;

    public static ArrayList<Contact> multiselect_list = new ArrayList<Contact>();
    android.view.ActionMode mActionMode;
    Menu context_menu;
    @BindView(R.id.image_empty)
    ImageView emptyImg;
    Contact contact;
    InvitationRVAdapter adapter;
    LinearLayoutManager mLayoutManager;
    List<Contact> contactsList;
    @BindView(R.id.listInvitation)
    RecyclerView listInvitation;
    ProgressDialog progressDialog;
    User current;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((BaseActivity) getActivity()).setOnBackPressedListener(this);
        contactsList = new ArrayList<>();
        progressDialog = new ProgressDialog(getContext());
        current = getCurrentUser();
        loadContact();


        super.setTitle("Contacts");


    }

    public void showContacts(List<Contact> l) {
        mLayoutManager = new LinearLayoutManager(getActivity());
        listInvitation.setLayoutManager(mLayoutManager);
        Log.v("HERE", "HERE");
        adapter = new InvitationRVAdapter(getActivity(), l);
        listInvitation.setAdapter(adapter);


        listInvitation.addOnItemTouchListener(new RecyclerItemClickListener(this.getContext(), listInvitation, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (isMultiSelect)
                    multi_select(position);
                else
                    Toast.makeText(getContext(), "Details Page", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemLongClick(View view, int position) {
                if (!isMultiSelect) {
                    multiselect_list = new ArrayList<Contact>();
                    isMultiSelect = true;

                    if (mActionMode == null) {
                        mActionMode = getActivity().startActionMode(mActionModeCallback);

                    }
                }

                multi_select(position);

            }
        }));
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_contacts_saved_fragment;


    }

    @Override
    public void onBackPressed() {
        DashboardFragment dashboardFragment = new DashboardFragment();
        getFragmentManager().beginTransaction().replace(R.id.frame_container, dashboardFragment).commit();
    }


    void loadContact() {
        contactsList.clear();

        progressDialog.setMessage("Loading Contacts  ...");
        progressDialog.show();
        Log.v("all ", "" + Utils.urlServer + "/contacts/all");
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                Request.Method.GET,
                Utils.urlServer + "/contacts/all",
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            Contact c = new Contact();
                            try {

                                c.setAdresse(response.getJSONObject(i).getString("Adresse"));
                                c.setFullName(response.getJSONObject(i).getString("Fullname"));
                                c.setNumTel(response.getJSONObject(i).getString("NumtelContact"));
                                c.setSexe(response.getJSONObject(i).getString("Sexe"));
                                c.setReligion(response.getJSONObject(i).getString("Religion"));
                                c.setEtatInvitation(response.getJSONObject(i).getInt("EtatContactInvit"));
                                c.setEmploi(response.getJSONObject(i).getString("Emploi"));
                                c.setAge(response.getJSONObject(i).getInt("Age"));
                                c.setEtatCivil(response.getJSONObject(i).getString("EtatCivil"));
                                c.setId_user(response.getJSONObject(i).getInt("id_user"));
                                Log.v("c", c.getFullName() + " " + c.getNumTel() + " " + c.getEtatInvitation());
                                contactsList.add(c);
                                System.out.println(c);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.v("NumberList", contactsList.size() + "");
                        if (contactsList.size() == 0) {
                            listInvitation.setVisibility(View.GONE);
                            emptyImg.setVisibility(View.VISIBLE);
                        } else {
                            listInvitation.setVisibility(View.VISIBLE);
                            emptyImg.setVisibility(View.GONE);
                        }
                        progressDialog.dismiss();

                        showContacts(contactsList);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Please Check your Internet Connection!", Toast.LENGTH_SHORT).show();
                        Log.v("ResponseServer", error.toString());


                    }
                }
        );

        // Add JsonObjectRequest to the RequestQueue
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }


    public void multi_select(int position) {
        if (mActionMode != null) {
            if (multiselect_list.contains(contactsList.get(position)))
                multiselect_list.remove(contactsList.get(position));
            else
                multiselect_list.add(contactsList.get(position));

            if (multiselect_list.size() > 0)
                mActionMode.setTitle("" + multiselect_list.size());
            else {

                mActionMode.setTitle(" ");
                mActionMode.finish();

            }

            refreshAdapter();

        }
    }

    public void refreshAdapter() {
        adapter.selected_usersList = multiselect_list;
        adapter.listProposition = contactsList;
        adapter.notifyDataSetChanged();
    }


    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            ((BaseActivity) getActivity()).getSupportActionBar().hide();
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_contact_multi_select, menu);
            context_menu = menu;
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

            return false; // Return false if nothing is done
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_AffecterContact:
                    Toast.makeText(getContext(), "join", Toast.LENGTH_SHORT).show();
                    Utils.showDialogCompanyAlert(getActivity());
                    //   alertDialogHelper.showAlertDialog("","Delete Contact","DELETE","CANCEL",1,false);
                    return true;
                case R.id.action_supprimerContact:
                    Toast.makeText(getContext(), "delete", Toast.LENGTH_SHORT).show();

                    //   alertDialogHelper.showAlertDialog("","Delete Contact","DELETE","CANCEL",1,false);
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

            mActionMode = null;
            isMultiSelect = false;
            multiselect_list = new ArrayList<Contact>();

            refreshAdapter();
            ((BaseActivity) getActivity()).getSupportActionBar().show();
        }


    };

    void sendSmsInvit(ArrayList<Contact> multiselect_list) {
        String smsMessage = "Hi," + current.getFullName().toString() + " has invited you to use SmsAdsPlatform app.Don't waste the time and download it";
        for (int i = 0; i < multiselect_list.size(); i++) {

            Log.v("selectList", "" + multiselect_list.get(i).getFullName());
            try {
                SmsManager smgr = SmsManager.getDefault();
                smgr.sendTextMessage(multiselect_list.get(i).getNumTel(), null, smsMessage, null, null);
                Toast.makeText(this.getContext(), "SMS Sent Successfully To " + multiselect_list.get(i).getFullName(), Toast.LENGTH_SHORT).show();
                UpdateEtat(multiselect_list.get(i));

            } catch (Exception e) {
                Toast.makeText(this.getContext(), " Please try again ,SMS Failed to Send to " + multiselect_list.get(i).getFullName(), Toast.LENGTH_SHORT).show();
            }
        }

    }


    private User getCurrentUser() {
        Gson gson = new Gson();
        shared = getActivity().getSharedPreferences(SHARED_NAME, MODE_PRIVATE);
        String jsonCurrent_user = shared.getString("current_user", "");
        User user = gson.fromJson(jsonCurrent_user, User.class);
        return user;
    }


    private void UpdateEtat(final Contact contact) {
        Log.v("Clicked", "clicked");
        progressDialog.show();

        String url = Utils.urlServer + "/contacts/update/" + contact.getNumTel();
        Utils.showProgressDialog(progressDialog, "SmsAdsPlatform", "Update Contacts..", false);
        StringRequest jsonObjectRequest = new StringRequest(
                Request.Method.PUT,
                url,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("ResponseServer", response.toString());


                        if (response.isEmpty()) {
                            progressDialog.dismiss();
                            //Utils.showAlert(getActivity(), "Update Contacts", "Failed to Update! ");
                        } else {
                            progressDialog.dismiss();
                            //Utils.showAlert(getActivity(), "Update Contacts", "Success to Opdate!");

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Please Check your Internet Connection!", Toast.LENGTH_SHORT).show();
                        // Do something when error occurred

                        Log.v("ResponseServer", error.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {


                Map<String, String> params = new HashMap<String, String>();
                contact.setEtatInvitation(1);
                params.put("tag", "new");

                params.put("EtatContactInvit", ""+contact.getEtatInvitation());

                return params;
            }

        };

        VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

    }
}


