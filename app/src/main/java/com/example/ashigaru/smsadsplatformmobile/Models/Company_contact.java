package com.example.ashigaru.smsadsplatformmobile.Models;

import java.math.BigInteger;

public class Company_contact {

    private int id;
    private BigInteger id_contact;
    private int id_company;
    private int id_user;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigInteger getId_contact() {
        return id_contact;
    }

    public void setId_contact(BigInteger id_contact) {
        this.id_contact = id_contact;
    }

    public int getId_company() {
        return id_company;
    }

    public void setId_company(int id_company) {
        this.id_company = id_company;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }
    public Company_contact(){}
    public Company_contact(int id, BigInteger id_contact, int id_company, int id_user) {
        this.id = id;
        this.id_contact = id_contact;
        this.id_company = id_company;
        this.id_user = id_user;
    }

    @Override
    public String toString() {
        return "Company_contact{" +
                "id=" + id +
                ", id_contact=" + id_contact +
                ", id_company=" + id_company +
                ", id_user=" + id_user +
                '}';
    }
}
