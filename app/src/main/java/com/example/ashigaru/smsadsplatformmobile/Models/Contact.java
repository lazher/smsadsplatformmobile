package com.example.ashigaru.smsadsplatformmobile.Models;

import java.io.Serializable;

public class Contact implements Serializable {


    private String FullName;
    private String NumTel;
    private int EtatInvitation;
    private String Sexe;
    private String EtatCivil;
    private String Religion;
    private int Age;
    private String Emploi;
    private String Adresse;
    private int id_user;


    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getNumTel() {
        return NumTel;
    }

    public void setNumTel(String numTel) {
        NumTel = numTel;
    }

    public int getEtatInvitation() {
        return EtatInvitation;
    }

    public void setEtatInvitation(int etatInvitation) {
        this.EtatInvitation = etatInvitation;
    }

    public String getSexe() {
        return Sexe;
    }

    public void setSexe(String sexe) {
        Sexe = sexe;
    }

    public String getEtatCivil() {
        return EtatCivil;
    }

    public void setEtatCivil(String etatCivil) {
        EtatCivil = etatCivil;
    }

    public String getReligion() {
        return Religion;
    }

    public void setReligion(String religion) {
        Religion = religion;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public String getEmploi() {
        return Emploi;
    }

    public void setEmploi(String emploi) {
        Emploi = emploi;
    }

    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String adresse) {
        Adresse = adresse;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }


    public Contact(String fullName, String numTel, int etatInvitation, String sexe, String etatCivil, String religion, int age, String emploi, String adresse, int id_user) {
        FullName = fullName;
        NumTel = numTel;
        EtatInvitation = etatInvitation;
        Sexe = sexe;
        EtatCivil = etatCivil;
        Religion = religion;
        Age = age;
        Emploi = emploi;
        Adresse = adresse;
        this.id_user = id_user;

    }

    public Contact(){}


}