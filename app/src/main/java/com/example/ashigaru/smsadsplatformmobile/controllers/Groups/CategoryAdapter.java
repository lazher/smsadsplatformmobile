package com.example.ashigaru.smsadsplatformmobile.controllers.Groups;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.example.ashigaru.smsadsplatformmobile.Models.Group;
import com.example.ashigaru.smsadsplatformmobile.R;

import java.util.List;

/**
 * Created by beyram on 20/11/17.
 */

public class CategoryAdapter extends ArrayAdapter<Group> {
    List<Group> hobbiesList ;
    private Context context;
    public CategoryAdapter(Context context, List<Group> hobbiesList) {
        super(context,0,hobbiesList);
        this.context = context;
        this.hobbiesList = hobbiesList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_category, parent, false);
            vh = new ViewHolder(convertView);
            convertView.setTag(vh);
        } else vh = (ViewHolder) convertView.getTag();


        return convertView;
    }

    private static final class ViewHolder {
        TextView description;

        public ViewHolder(View v) {
            description = (TextView) v.findViewById(R.id.description);
        }
    }
}