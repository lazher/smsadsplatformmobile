package com.example.ashigaru.smsadsplatformmobile.controllers.Groups;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.android.volley.toolbox.StringRequest;
import com.example.ashigaru.smsadsplatformmobile.Models.User;

import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 *
 */

public class AlertDialogAddCompany extends DialogFragment implements View.OnClickListener {

    static String txtMessage, txtTitle;
    View rootView;
    Button alertPositiveButton;
    Button alertNegativeButton;

    TextView alertMessage, alertTitle;

    EditText edtDescription;
    User current;
    private ProgressDialog progress;
    private  static  final String SHARED_NAME = "user_prefs" ;
    SharedPreferences shared;
    public AlertDialogAddCompany() {
        // Required empty public constructor
    }

    public static AlertDialogAddCompany newInstance() {
        AlertDialogAddCompany f = new AlertDialogAddCompany();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.alert_add_company, container, false);
        alertPositiveButton = (Button) rootView.findViewById(R.id.alertPositiveButton);
        alertNegativeButton = (Button) rootView.findViewById(R.id.alertNegativeButton);

        edtDescription = (EditText) rootView.findViewById(R.id.edt_description_hobbies);
        alertMessage = (TextView) rootView.findViewById(R.id.alertMessage);
        alertTitle = (TextView) rootView.findViewById(R.id.alertTitle);
        alertPositiveButton.setOnClickListener(this);
        alertNegativeButton.setOnClickListener(this);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RelativeLayout relativeLayout = (RelativeLayout) rootView.findViewById(R.id.alertBG);
        relativeLayout.setBackground(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        current = getCurrentUser();
        System.out.println(current);
        progress = new ProgressDialog(getActivity());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.alertPositiveButton : {
               // String username = current.ge();


                String description =  edtDescription.getText().toString();
               // Log.v("USERNAMEEE" , username);
              //  Log.v("categorie" , categorie);
                Log.v("description" , description);
                if(description == "") {
                    Utils.showAlert(getActivity(),"Sms Ads Platform" , "Please add Title");
                } else {
                    saveHobbies(current  , description) ;
                }
                break;
            }
            case R.id.alertNegativeButton : {
                getDialog().dismiss();
            }
        }
    }

    public void showAlert(Activity activity) {
        AlertDialogAddCompany newFragment = AlertDialogAddCompany.newInstance();
        newFragment.show(activity.getFragmentManager(), "dialogHobbies");
    }

    private User getCurrentUser() {
        Gson gson = new Gson();
        shared = getActivity().getSharedPreferences(SHARED_NAME, MODE_PRIVATE);
        String jsonCurrent_user = shared.getString("current_user", "");
        User user = gson.fromJson(jsonCurrent_user, User.class);
        return  user;
    }

    void saveHobbies(final User user, final String description) {
        if(!description.isEmpty()) {
            Utils.showProgressDialog(progress, "Why Not", "company Hobbies..", false);

            StringRequest jsonObjectRequest = new StringRequest(
                    Request.Method.POST,
                    Utils.urlServer +"/company/new",

                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.v("ResponseServer" , response.toString());
                                    progress.dismiss();
                                    Toast.makeText(getActivity(),"company was added !" , Toast.LENGTH_SHORT).show();
                                    getDialog().dismiss();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progress.dismiss();
                            Toast.makeText(getActivity(), "Failed , Check Your connexion !", Toast.LENGTH_SHORT).show();
                            Log.v("ResponseServer", error.toString());
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("tag", "new");
                    params.put("idUser", " "+user.getId());
                    params.put("titre", ""+description.toString());
                    return params;
                }
            };

            // Add JsonObjectRequest to the RequestQueue
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        } else {
            Toast.makeText(getActivity(),"Please fill the description" , Toast.LENGTH_SHORT).show();
        }

    }
    }
