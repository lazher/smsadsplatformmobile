package com.example.ashigaru.smsadsplatformmobile.controllers.LoginSignUp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseFragment;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;
import com.kyleduo.switchbutton.SwitchButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnItemSelected;
import timber.log.Timber;

public class SignUpFragment extends BaseFragment implements View.OnClickListener {


    @BindView(R.id.gologinButton)
    Button loginBtn;
    @BindView(R.id.usernameLogin)
    EditText usernameEdt;
    @BindView(R.id.passwordLogin)
    EditText passwordEdt;
    @BindView(R.id.fullNameLogin)
    EditText Fullname;

    @BindView(R.id.dpDate)
    DatePicker DateNaissance;
    @BindView(R.id.NumTel)
    EditText NumTel;
    @BindView(R.id.AdresseUser)
    EditText Adresse;
    @BindView(R.id.Emploi)
    EditText Emploi;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.sexeSwitch)
    SwitchButton sexeSwitch;
    @BindView(R.id.signupButton)
    Button signupButton;
    private ProgressDialog progress;
    String sexeValue="";
    @BindView(R.id.ReligionUser)
    Spinner religion;
    @BindView(R.id.EtatCivilUser)
    Spinner etatCivil;

    public SignUpFragment() {
        // Required empty public constructor
    }
    @Override
    public int getLayoutId() {
        return R.layout.fragment_sign_up;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginBtn.setOnClickListener(this);
        signupButton.setOnClickListener(this);
        loadSpinnerIdTypes();
        progress = new ProgressDialog(getContext());
        sexeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sexeValue="male";
                } else {
                    sexeValue="female";
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gologinButton : {
                getFragmentManager().beginTransaction().replace(R.id.logsign_container , new LoginFragment()).commit();
                break;
            }
            case R.id.signupButton : {
                signUP();
                break;
            }
        }
    }
    private void loadSpinnerIdTypes() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.typr_religion , android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        religion.setAdapter(adapter);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(getContext(),
                R.array.typr_etatCivil , android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        etatCivil.setAdapter(adapter1);
    }
    @OnItemSelected(R.id.ReligionUser)
    void onItemSelected(int position) {
        Timber.d("Element selected %s ", religion.getItemAtPosition(position));
    }
    private void signUP() {
        Log.v("Clicked" , "clicked");
        String username = usernameEdt.getText().toString();
        String pass = passwordEdt.getText().toString();
        String fullname = Fullname.getText().toString();
        if(username.length() < 8) {
            Toast.makeText(getContext(),"Username should contains at least 8 caracters" , Toast.LENGTH_SHORT).show();
        } else {
            if (pass.length() < 8) {
                Toast.makeText(getContext(),"Password should contains at least 8 caracters" , Toast.LENGTH_SHORT).show();
            } else {
                if(fullname.length() < 8) {
                    Toast.makeText(getContext(),"FullName should contains at least 8 caracters" , Toast.LENGTH_SHORT).show();
                } else {
                    String url = Utils.urlServer +"/user_1/new" ;
                    Utils.showProgressDialog(progress, "SmsAdsPlatform", "Sign UP..", false);
                    StringRequest jsonObjectRequest = new StringRequest(
                            Request.Method.POST,
                            url,

                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.v("ResponseServer" , response.toString());


                                        if (response.isEmpty()) {
                                            progress.dismiss();
                                            Utils.showAlert(getActivity() , "SIGN UP" , "Failed to SignUp! User already exist" );
                                        } else {
                                            progress.dismiss();
                                            Utils.showAlert(getActivity() , "SIGN UP" , "Success to SignUp!" );

                                        }



                                }
                            },
                            new Response.ErrorListener(){
                                @Override
                                public void onErrorResponse(VolleyError error){
                                    progress.dismiss();
                                    Toast.makeText(getContext(),"Please Check your Internet Connection!" , Toast.LENGTH_SHORT).show();
                                    // Do something when error occurred

                                    Log.v("ResponseServer" , error.toString());
                                }
                            })
                                    {    @Override
                                    public Map<String, String> getHeaders() throws AuthFailureError {
                                        HashMap<String, String> headers = new HashMap<String, String>();
                                        headers.put("Content-Type", "application/x-www-form-urlencoded");
                                        return headers;
                                    }

                                    @Override
                                    protected Map<String, String> getParams() {
                                        int   day  = DateNaissance.getDayOfMonth();
                                        int   month= DateNaissance.getMonth();
                                        int   year = DateNaissance.getYear();
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.set(year, month, day);

                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                        String formatedDate = sdf.format(calendar.getTime());
                                        Map<String, String> params = new HashMap<String, String>();

                                        params.put("tag", "new");
                                        params.put("date", " "+formatedDate.toString());
                                        params.put("Fullname", Fullname.getText().toString());
                                        params.put("Numtel", NumTel.getText().toString());
                                        params.put("DateNaissance", formatedDate.toString() );
                                        params.put("Emploi", Emploi.getText().toString());
                                        params.put("Adresse", Adresse.getText().toString());
                                        params.put("Email", email.getText().toString());

                                        params.put("Sexe" ,sexeValue.toString());
                                      //  params.put("id_user","1");
                                        params.put("Username", usernameEdt.getText().toString());
                                        params.put("Password", passwordEdt.getText().toString());
                                        params.put("EtatCivil", etatCivil.getSelectedItem().toString());
                                        params.put("Religion",religion.getSelectedItem().toString());
                                        return params;
                                    }

                                };

                    VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
                }
            }
        }

    }
}

