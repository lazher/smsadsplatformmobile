package com.example.ashigaru.smsadsplatformmobile.controllers.Groups;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.example.ashigaru.smsadsplatformmobile.Models.Group;
import com.example.ashigaru.smsadsplatformmobile.Models.Contact;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;
import com.example.ashigaru.smsadsplatformmobile.controllers.Contacts.AlertDialogListCompany;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.ashigaru.smsadsplatformmobile.controllers.Contacts.ContactsSavedFragment.multiselect_list;

/**

 */

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.ViewHolder> {

    List<Group> hobbiesList;
    private Context mContext;
    View rowView;
    private ProgressDialog progress;

    public CompanyAdapter(Context mContext, List<Group> hobbiesList) {
        this.hobbiesList = hobbiesList;
        this.mContext = mContext;
    }

    @Override
    public CompanyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_company, parent, false);
        CompanyAdapter.ViewHolder viewHolder = new CompanyAdapter.ViewHolder(rowView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CompanyAdapter.ViewHolder viewHolder, int position) {
        Group comp = hobbiesList.get(position);

        viewHolder.hobbies_txt.setText(comp.getTitre());
        viewHolder.hobbies_img.setImageResource(comp.getImgResource());

        // mItemManger.bindView(viewHolder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return (null != hobbiesList ? hobbiesList.size() : 0);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView hobbies_txt;

        ImageView hobbies_img;

        public ViewHolder(View view) {
            super(view);
            this.hobbies_txt = view.findViewById(R.id.hobbies_txt);
            this.hobbies_img = view.findViewById(R.id.hobbies_img);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
           int idCompany= hobbiesList.get(getAdapterPosition()).getId();
            Log.v("click", "itemmm " + hobbiesList.get(getAdapterPosition()).getId());
            Log.v("test", "" + multiselect_list.size());
            for(int i=0; i < multiselect_list.size() ; i++){
            Affecter(idCompany,multiselect_list.get(i));
                AlertDialogListCompany.a.dismiss();
                Toast.makeText(v.getContext(), "Contacts add to "+hobbiesList.get(getAdapterPosition()).getTitre()+" successfully.", Toast.LENGTH_SHORT).show();}
        }


    }


    public void updateData(final List<Group> c) {
        hobbiesList = new ArrayList<>();
        hobbiesList.addAll(c);
    }


    private void Affecter(final int id, final Contact c) {
        Log.v("Clicked", "clicked");

        progress = new ProgressDialog(mContext);

      //  Utils.showProgressDialog(progress, "SmsAdsPlatform", "Sign UP..", false);

            String url = Utils.urlServer + "/company_contact/new";

        StringRequest jsonObjectRequest = new StringRequest(
                Request.Method.POST,
                url,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("ResponseServer", response.toString());


                        if (response.isEmpty()) {
                            progress.dismiss();
                         //   Utils.showAlert(, "SIGN UP", "Failed to SignUp! User already exist");
                        } else {
                            progress.dismiss();
                        //    Utils.showAlert(  , "SIGN UP", "Success to SignUp!");

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                      //  Toast.makeText(getContext(), "Please Check your Internet Connection!", Toast.LENGTH_SHORT).show();
                        // Do something when error occurred

                        Log.v("ResponseServer", error.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tag", "new");
                params.put("id_company", ""+id);
                params.put("id_contact", c.getNumTel());
                params.put("id_user", ""+c.getId_user());
                return params;
            }

        };

        VolleySingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }
    }





