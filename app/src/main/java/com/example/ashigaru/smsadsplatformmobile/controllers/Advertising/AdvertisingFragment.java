package com.example.ashigaru.smsadsplatformmobile.controllers.Advertising;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseActivity;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseFragment;
import com.example.ashigaru.smsadsplatformmobile.Models.Advertising;
import com.example.ashigaru.smsadsplatformmobile.Models.Contact;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;
import com.example.ashigaru.smsadsplatformmobile.controllers.Dashboard.DashboardFragment;
import com.example.ashigaru.smsadsplatformmobile.controllers.Invitation.InvitationRVAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import butterknife.BindView;

public class AdvertisingFragment extends BaseFragment {
    //List<Contact> propositionList;
    @BindView(R.id.image_empty)
    ImageView emptyImg;
    Advertising advertising;
    AdvertisingAdapter adapter;
    LinearLayoutManager mLayoutManager;
    List<Advertising> advertisingList;
    @BindView(R.id.listInvitation)
    RecyclerView listAdvertising;
    ProgressDialog progressDialog;
    public SwipeRefreshLayout mSwipeRefreshLayout;

    void loadContact() {
        emptyImg.setVisibility(View.GONE);
        advertising=new Advertising();
        advertising.setPubText("Maghzaoui");
        advertising.setDate(Date.valueOf("1990-11-10"));
        advertising.setId(5);

        advertisingList.add(advertising);
        advertisingList.add(advertising);
        advertisingList.add(advertising);
        advertisingList.add(advertising);
        Log.v("invitation","contact : "+advertising.getPubText()+" "+advertising.getId()+" "+advertising.getDate());
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((BaseActivity) getActivity()).setOnBackPressedListener(this);
        advertisingList=new ArrayList<Advertising>();
        progressDialog = new ProgressDialog(getContext());
        loadFriends();
        super.setTitle("Advetising");

        mSwipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swifeRefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadFriends();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }
    public void showAdvertising(List<Advertising> l) {
        mLayoutManager = new LinearLayoutManager(getActivity());
        listAdvertising.setLayoutManager(mLayoutManager);
        Log.v("HERE", "HERE");
        adapter = new AdvertisingAdapter(getActivity(), l);
        listAdvertising.setAdapter(adapter);
    }
    @Override
    public int getLayoutId() {
        return R.layout.activity_invitation_fragment;


    }

    @Override
    public void onBackPressed() {
        DashboardFragment dashboardFragment = new DashboardFragment();
        getFragmentManager().beginTransaction().replace(R.id.frame_container, dashboardFragment).commit();
    }





    void loadFriends() {
        advertisingList.clear();

        progressDialog.setMessage("Loading Advertising...");
        progressDialog.show();
        Log.v("allpubs",""+Utils.urlServer + "/pubs/all");
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                Request.Method.GET,
                Utils.urlServer + "/pubs/all",
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            Advertising ads = new Advertising();



                            try {
                                ads.setId(response.getJSONObject(i).getInt("id"));
                                ads.setPubText(response.getJSONObject(i).getString("pubText"));
                                ads.setUrl(response.getJSONObject(i).getString("url"));
                                String date = response.getJSONObject(i).getString("date") ;
                                 System.out.println(date);

                                java.util.Date date1 = null;
                                String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
                                try {
                                    date1 = new SimpleDateFormat(DATE_FORMAT_PATTERN).parse(date);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                                ads.setDate(date1);
                                Log.v("ads" , ads.getPubText()+" "+ads.getId()+" "+ads.getDate()) ;
                                advertisingList.add(ads);
                                showAdvertising(advertisingList);
                                System.out.println(ads);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.v("NumberList" , advertisingList.size()+"");
                        if(advertisingList.size() == 0) {
                            listAdvertising.setVisibility(View.GONE);
                            emptyImg.setVisibility(View.VISIBLE);
                        } else {
                            listAdvertising.setVisibility(View.VISIBLE);
                            emptyImg.setVisibility(View.GONE);
                        }
                         progressDialog.dismiss();




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        progressDialog.dismiss();
                        Toast.makeText(getContext(),"Please Check your Internet Connection!" , Toast.LENGTH_SHORT).show();
                        Log.v("ResponseServer", error.toString());


                    }
                }
        );

        // Add JsonObjectRequest to the RequestQueue
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }

}
