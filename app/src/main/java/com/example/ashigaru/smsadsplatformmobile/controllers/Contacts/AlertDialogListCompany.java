package com.example.ashigaru.smsadsplatformmobile.controllers.Contacts;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.ashigaru.smsadsplatformmobile.Models.Group;
import com.example.ashigaru.smsadsplatformmobile.Models.User;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;
import com.example.ashigaru.smsadsplatformmobile.controllers.Groups.CompanyAdapter;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

import static android.content.Context.MODE_PRIVATE;

/**
 *  
 */

public class AlertDialogListCompany extends DialogFragment implements View.OnClickListener {
    LinearLayoutManager mLayoutManager;
    @BindView(R.id.listCompany)
    RecyclerView listViewHobbies;
    CompanyAdapter adapter;
    static String txtMessage, txtTitle;
    View rootView;
    List<Group> hobbiesList;
    Button alertNegativeButton;
    public static Dialog a  ;

    TextView alertMessage, alertTitle;

    EditText edtDescription;
    User current;
    private ProgressDialog progress;
    private  static  final String SHARED_NAME = "user_prefs" ;
    SharedPreferences shared;
    public AlertDialogListCompany() {
        // Required empty public constructor
    }

    public static AlertDialogListCompany newInstance() {
        AlertDialogListCompany f = new AlertDialogListCompany();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.alert_list_company, container, false);
        listViewHobbies = (RecyclerView) rootView.findViewById(R.id.listCompany);
        alertNegativeButton = (Button) rootView.findViewById(R.id.alertNegativeButton);
        alertTitle = (TextView) rootView.findViewById(R.id.alertTitle);
        hobbiesList = new ArrayList<>();
        loadCompany();
        a =   this.getDialog();
        showAdvertising(hobbiesList);
        alertNegativeButton.setOnClickListener(this);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RelativeLayout relativeLayout = (RelativeLayout) rootView.findViewById(R.id.alertBG);
        relativeLayout.setBackground(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return rootView;
    }
    public void showAdvertising(List<Group> l) {
        mLayoutManager = new LinearLayoutManager(getActivity());
        listViewHobbies.setLayoutManager(mLayoutManager);
        Log.v("HERE", "HERE");
        adapter = new CompanyAdapter(getActivity(), l);
        listViewHobbies.setAdapter(adapter);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        current = getCurrentUser();
        System.out.println(current);
        progress = new ProgressDialog(getActivity());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.alertPositiveButton : {
               // String username = current.ge();


                String description =  edtDescription.getText().toString();
               // Log.v("USERNAMEEE" , username);
              //  Log.v("categorie" , categorie);
                Log.v("description" , description);
                if(description == "") {
                    Utils.showAlert(getActivity(),"Sms Ads Platform" , "Please add Title");
                } else {
                    saveHobbies(current  , description) ;
                }
                break;
            }
            case R.id.alertNegativeButton : {
                getDialog().dismiss();
            }
        }
    }

    public void showAlert(Activity activity) {
        AlertDialogListCompany newFragment = AlertDialogListCompany.newInstance();
        newFragment.show(activity.getFragmentManager(), "dialogHobbies");
    }

    private User getCurrentUser() {
        Gson gson = new Gson();
        shared = getActivity().getSharedPreferences(SHARED_NAME, MODE_PRIVATE);
        String jsonCurrent_user = shared.getString("current_user", "");
        User user = gson.fromJson(jsonCurrent_user, User.class);
        return  user;
    }

    void saveHobbies(final User user, final String description) {
        if(!description.isEmpty()) {
            Utils.showProgressDialog(progress, "Why Not", "company Hobbies..", false);

            StringRequest jsonObjectRequest = new StringRequest(
                    Request.Method.POST,
                    Utils.urlServer +"/company/new",

                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.v("ResponseServer" , response.toString());
                                    progress.dismiss();
                                    Toast.makeText(getActivity(),"company was added !" , Toast.LENGTH_SHORT).show();
                                    getDialog().dismiss();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progress.dismiss();
                            Toast.makeText(getActivity(), "Failed , Check Your connexion !", Toast.LENGTH_SHORT).show();
                            Log.v("ResponseServer", error.toString());
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("tag", "new");
                    params.put("idUser", " "+user.getId());
                    params.put("titre", ""+description.toString());
                    return params;
                }
            };

            // Add JsonObjectRequest to the RequestQueue
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        } else {
            Toast.makeText(getActivity(),"Please fill the description" , Toast.LENGTH_SHORT).show();
        }

    }
    void loadCompany() {

//        hobbiesList.clear();

        String url = Utils.urlServer + "/company/all";
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            Group comp =new Group() ;
                            try {
                                comp.setId(response.getJSONObject(i).getInt("id"));
                                comp.setIdUser(response.getJSONObject(i).getInt("idUser"));
                                comp.setTitre(response.getJSONObject(i).getString("titre"));
                                comp.setColorRes(R.string.sportstring);
                                comp.setImgResource(R.drawable.friends2);
                                comp.setType("SPORT");
                                hobbiesList.add(comp);
                                Log.v("size1",""+String.valueOf(hobbiesList.size()));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.v("ResponseServer", response.toString());

                        showAdvertising(hobbiesList);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity().getApplicationContext(),"Please Check your Internet Connection!" , Toast.LENGTH_SHORT).show();

                        Log.v("ResponseServer", error.toString());


                    }
                }
        );

        // Add JsonObjectRequest to the RequestQueue
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView hobbies_txt;

        ImageView hobbies_img;

        public ViewHolder(View view) {
            super(view);
            this.hobbies_txt =  view.findViewById(R.id.hobbies_txt);
            this.hobbies_img =  view.findViewById(R.id.hobbies_img);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.linearLayoutRefuse: {

                    break;
                }
                case R.id.linearLayoutAccept: {

                    break;
                }
            }
        }


    }
}
