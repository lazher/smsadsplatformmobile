package com.example.ashigaru.smsadsplatformmobile.controllers;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import android.os.Handler;
import android.support.annotation.NonNull;

import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;

import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.ashigaru.smsadsplatformmobile.Base.BaseActivity;
import com.example.ashigaru.smsadsplatformmobile.Models.User;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.controllers.Advertising.AdvertisingFragment;
import com.example.ashigaru.smsadsplatformmobile.controllers.Groups.CompanyFragment;
import com.example.ashigaru.smsadsplatformmobile.controllers.Contacts.ContactsTabActivity;
import com.example.ashigaru.smsadsplatformmobile.controllers.Dashboard.DashboardFragment;
import com.example.ashigaru.smsadsplatformmobile.controllers.Invitation.InvitationFragment;
import com.example.ashigaru.smsadsplatformmobile.controllers.LoginSignUp.LoginSignupActivity;
import com.example.ashigaru.smsadsplatformmobile.controllers.Stats.StatFragment;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;


public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    SharedPreferences sharedPreferences;
    private static final String SHARED_NAME = "user_prefs";
    User current;
    SharedPreferences shared;

    @Override
    public int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shared = getSharedPreferences(SHARED_NAME, MODE_PRIVATE);
        current = getCurrentUser();
        setUpActionBar();
        DashboardFragment dashboardFragment = new DashboardFragment();
        Log.d("Firebase", "token "+ FirebaseInstanceId.getInstance().getToken());
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, dashboardFragment).commit();
        sharedPreferences = this.getSharedPreferences(SHARED_NAME, MODE_PRIVATE);
}


    private void setUpActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
       // layoutHome = drawer.findViewById(R.id.frame_container);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icn_menu1, getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerLayout = navigationView.getHeaderView(0);
        TextView headerName = (TextView) headerLayout.findViewById(R.id.nameUser);
        //CircleImageView headerImg = (CircleImageView) headerLayout.findViewById(R.id.userPic);
        headerName.setText(current.getFullName().toString());
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawer.closeDrawer(GravityCompat.START);
        switch (item.getItemId()) {
            case R.id.companyMenu : {
                CompanyFragment CompanyFragment = new CompanyFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, CompanyFragment).commit();
                break;
            }
            case R.id.dashboardMenu : {
                DashboardFragment dashboardFragment = new DashboardFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, dashboardFragment).commit();
                break;
            }

            case R.id.friendsMenu : {
                ContactsTabActivity ContactsTab = new ContactsTabActivity();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, ContactsTab).commit();
                break;
            }
            case R.id.invitationMenu : {
                InvitationFragment invitationFragment = new InvitationFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, invitationFragment).commit();
                break;
            }
            case R.id.activitiesMenu : {
                AdvertisingFragment advertisingFragment = new AdvertisingFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, advertisingFragment,"ActivFrag").commit();
                break;
            }
            case R.id.statMenu : {
                StatFragment StatFragment = new StatFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, StatFragment,"ActivFrag").commit();
                break;
            }
            case R.id.logoutMenu : {
                final ProgressDialog progressBar = new ProgressDialog(this);
                progressBar.setMessage("Logout...");
                progressBar.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        progressBar.dismiss();

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.clear();
                        editor.commit();



                        finish();
                        startActivity(new Intent(HomeActivity.this,LoginSignupActivity.class));
                    }
                }, 3000);

                break;
            }
        }
        return true;
    }
    private User getCurrentUser() {
        Gson gson = new Gson();
        String jsonCurrent_user = shared.getString("current_user", "");
        User user = gson.fromJson(jsonCurrent_user, User.class);
        Log.d("test", "getCurrentUser: "+user);
        return  user;
    }
}