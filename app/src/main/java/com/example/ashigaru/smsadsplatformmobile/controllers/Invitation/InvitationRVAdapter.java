package com.example.ashigaru.smsadsplatformmobile.controllers.Invitation;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import com.example.ashigaru.smsadsplatformmobile.Models.Contact;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by beyram on 04/12/17.
 */

public class InvitationRVAdapter extends RecyclerSwipeAdapter<InvitationRVAdapter.ViewHolder> {

    public List<Contact> listProposition;
    public ArrayList<Contact> selected_usersList=new ArrayList<>();
    View rowView;
    private Context mContext;

    public InvitationRVAdapter(Context mContext, List<Contact> listProposition) {
        this.listProposition = listProposition;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invitation, parent, false);
        ViewHolder viewHolder = new ViewHolder(rowView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        Contact proposition = listProposition.get(position);

            viewHolder.swipeElememnt.setVisibility(View.VISIBLE);
            viewHolder.linearLayoutAccept.setVisibility(View.VISIBLE);
            viewHolder.linearLayoutRefuse.setVisibility(View.VISIBLE);

        viewHolder.userName.setText(proposition.getFullName());

         viewHolder.dateInvit.setText(2000+"-"+11+"-"+11);
        viewHolder.subjectInvit.setText("Subject: " +"Invite him/her");

        if(selected_usersList.contains(listProposition.get(position)))
            viewHolder.ll_listitem.setBackgroundColor(ContextCompat.getColor(mContext, R.color.txtPurpleLight));
        else
            viewHolder.ll_listitem.setBackgroundColor(ContextCompat.getColor(mContext, R.color.list_item_normal_state));

        viewHolder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {

            @Override
            public void onOpen(SwipeLayout layout) {
                mItemManger.closeAllExcept(layout);
            }

        });

        mItemManger.bindView(viewHolder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return (null != listProposition ? listProposition.size() : 0);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeInvitation;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout linearLayoutRefuse, linearLayoutAccept,swipeElememnt,ll_listitem;
        SwipeLayout swipeLayout;
        TextView timeInvit;
        TextView dateInvit;
        TextView subjectInvit;
        TextView userName;
        CircleImageView imgUser;

        public ViewHolder(View view) {
            super(view);
            this.ll_listitem= view.findViewById(R.id.ll_listitem);
            this.swipeLayout =  view.findViewById(R.id.swipeInvitation);
            this.timeInvit =  view.findViewById(R.id.timeInvit);
            this.dateInvit = view.findViewById(R.id.dateInvit);
            this.subjectInvit =  view.findViewById(R.id.subjectInvit);
            this.userName =  view.findViewById(R.id.userName);
            this.imgUser =  view.findViewById(R.id.imgUser);
            this.swipeElememnt = view.findViewById(R.id.swipeElememnt);
            this.linearLayoutRefuse =  view.findViewById(R.id.linearLayoutRefuse);
            this.linearLayoutAccept =  view.findViewById(R.id.linearLayoutAccept);
            linearLayoutAccept.setOnClickListener(this);
            linearLayoutRefuse.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.linearLayoutRefuse: {

                    swipeLayout.close();
                    break;
                }
                case R.id.linearLayoutAccept: {

                    swipeLayout.close();
                    break;
                }
            }
        }

        private void acceptInvitation(final Contact id) {
            final ProgressDialog progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Accepting...");
            progressDialog.show();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.GET,
                    Utils.urlServer +"/propositionService/acceptProposition?idInvit=",
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            progressDialog.dismiss();
                            Log.v("ResponseServer" , response.toString());
                            Toast.makeText(mContext,"Invitation was Accepted", Toast.LENGTH_LONG).show();
                            listProposition.remove(id);
                            notifyDataSetChanged();
                        }
                    },
                    new Response.ErrorListener(){
                        @Override
                        public void onErrorResponse(VolleyError error){
                            progressDialog.dismiss();
                            Toast.makeText(mContext,"Please Check your Internet Connection!" , Toast.LENGTH_SHORT).show();
                            Log.v("ResponseServer" , error.toString());

                        }
                    }
            );
            // Add JsonObjectRequest to the RequestQueue
            VolleySingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
        }

        private void refuseInvitation(final Contact id) {
            final ProgressDialog progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Accepting...");
            progressDialog.show();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.GET,
                    Utils.urlServer +"/propositionService/refuseProposition?idInvit=",
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            progressDialog.dismiss();
                            Toast.makeText(mContext,"Invitation was Refused", Toast.LENGTH_LONG).show();
                            listProposition.remove(id);
                            notifyDataSetChanged();
                        }
                    },
                    new Response.ErrorListener(){
                        @Override
                        public void onErrorResponse(VolleyError error){
                            progressDialog.dismiss();
                            Toast.makeText(mContext,"Please Check your Internet Connection!" , Toast.LENGTH_SHORT).show();
                            Log.v("ResponseServer" , error.toString());
                        }
                    }
            );
            // Add JsonObjectRequest to the RequestQueue
            VolleySingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
        }
    }



    public void updateData(final List<Contact> propositions ) {
        listProposition= new ArrayList<>();
        listProposition.addAll(propositions);
    }
}
