package com.example.ashigaru.smsadsplatformmobile.controllers.Groups;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import com.example.ashigaru.smsadsplatformmobile.Base.BaseActivity;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseFragment;
import com.example.ashigaru.smsadsplatformmobile.Models.Group;
import com.example.ashigaru.smsadsplatformmobile.Models.User;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;
import com.example.ashigaru.smsadsplatformmobile.controllers.Dashboard.DashboardFragment;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static android.content.Context.MODE_PRIVATE;


public class CompanyFragment extends BaseFragment implements View.OnClickListener {

    CompanyAdapter adapter;
    LinearLayoutManager mLayoutManager;
    @BindView(R.id.listViewHobbies) RecyclerView listViewCompany;

    @BindView(R.id.addHobbiesBtn) FloatingActionButton addCompanyBtn;

    List<Group> groupsList;
    ArrayList<String> hobbies;
    User current;

    ProgressDialog progressDialog;


    private static final String SHARED_NAME = "user_prefs";
    SharedPreferences shared;
      void loads(){
      Group c =new Group();
     c.setTitre("Group");
     c.setImgResource(R.drawable.friends);
     c.setColorRes(R.string.gamingstring);
     c.setId(1);
     c.setIdUser(1);
          groupsList.add(c);
          groupsList.add(c);
          groupsList.add(c);
          groupsList.add(c);
          groupsList.add(c);
          groupsList.add(c);}

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((BaseActivity) getActivity()).setOnBackPressedListener(this);
       // addHobbiesBtn.setOnClickListener(this);
        groupsList = new ArrayList<>();
        progressDialog = new ProgressDialog(getContext());
        current = getCurrentUser();
        System.out.println(current);
        loadCompany();
        addCompanyBtn.setOnClickListener(this);
        Log.v("size2",""+String.valueOf(groupsList.size()));
        super.setTitle("My Groups");

       // listViewHobbies.setOnItemClickListener(this);

    }

    public void showAdvertising(List<Group> l) {
        mLayoutManager = new LinearLayoutManager(getActivity());
        listViewCompany.setLayoutManager(mLayoutManager);
        Log.v("HERE", "HERE");
        adapter = new CompanyAdapter(getActivity(), l);
        listViewCompany.setAdapter(adapter);
    }
    @Override
    public int getLayoutId() {
        return R.layout.fragment_company;
    }
    @Override
    public void onBackPressed() {
        DashboardFragment dashboardFragment = new DashboardFragment();
        getFragmentManager().beginTransaction().replace(R.id.frame_container, dashboardFragment).commit();
    }







    void loadCompany() {

//        hobbiesList.clear();
        progressDialog.setMessage("Loading Groups...");
        progressDialog.show();
        String url = Utils.urlServer + "/company/all";
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            Group comp =new Group() ;
                            try {
                                comp.setId(response.getJSONObject(i).getInt("id"));
                                comp.setIdUser(response.getJSONObject(i).getInt("idUser"));
                                comp.setTitre(response.getJSONObject(i).getString("titre"));
                                comp.setColorRes(R.string.sportstring);
                                comp.setImgResource(R.drawable.friends2);
                                comp.setType("SPORT");
                                groupsList.add(comp);
                                Log.v("size1",""+String.valueOf(groupsList.size()));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.v("ResponseServer", response.toString());
                        progressDialog.dismiss();
                        showAdvertising(groupsList);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(),"Please Check your Internet Connection!" , Toast.LENGTH_SHORT).show();

                        Log.v("ResponseServer", error.toString());


                    }
                }
        );

        // Add JsonObjectRequest to the RequestQueue
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

    }


    private User getCurrentUser() {
        Gson gson = new Gson();
        shared = getActivity().getSharedPreferences(SHARED_NAME, MODE_PRIVATE);
        String jsonCurrent_user = shared.getString("current_user", "");
        User user = gson.fromJson(jsonCurrent_user, User.class);
        return  user;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addHobbiesBtn: {
                Log.v("Clicked", "Clicked");
                Utils.showDialogHobbiesAlert(getActivity());
                break;
            }
        }
    }
}
