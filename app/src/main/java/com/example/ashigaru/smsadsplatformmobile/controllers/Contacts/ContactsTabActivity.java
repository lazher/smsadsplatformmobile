package com.example.ashigaru.smsadsplatformmobile.controllers.Contacts;

import android.app.ProgressDialog;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseActivity;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseFragment;
import com.example.ashigaru.smsadsplatformmobile.Models.Contact;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.controllers.Dashboard.DashboardFragment;


import java.util.ArrayList;
import java.util.List;

public class ContactsTabActivity extends BaseFragment {


    //This is our tablayout
    private TabLayout tabLayout;

    //This is our viewPager
    private ViewPager viewPager;


    //Fragments


    ContactFragment contactsFragment;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        super.setTitle("Contacts");
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tablayout);

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        final ViewPagerAdapter adapter = new ViewPagerAdapter
                (getActivity().getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_contacts_tab;


    }

    @Override
    public void onBackPressed() {

    }


}
