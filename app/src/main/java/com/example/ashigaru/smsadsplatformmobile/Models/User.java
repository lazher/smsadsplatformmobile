package com.example.ashigaru.smsadsplatformmobile.Models;

import android.widget.DatePicker;

import java.util.Date;

public class User {
    private int id;
    private String UserName;
    private String FullName;
    private String LastName;
    private String FirstName;
    private String Adresse;
    private String Sexe;
    private String Password;
    private String EtatCivil;
    private String Religion;
    private Date DateNaissance;
    private String EmploiTravailOccupation;
    private Integer NumTel ;
    private  int Balance;
    private String Email;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String adresse) {
        Adresse = adresse;
    }

    public String getSexe() {
        return Sexe;
    }

    public void setSexe(String sexe) {
        Sexe = sexe;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getEtatCivil() {
        return EtatCivil;
    }

    public void setEtatCivil(String etatCivil) {
        EtatCivil = etatCivil;
    }

    public String getReligion() {
        return Religion;
    }

    public void setReligion(String religion) {
        Religion = religion;
    }

    public Date getDateNaissance() {
        return DateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        DateNaissance = dateNaissance;
    }

    public String getEmploiTravailOccupation() {
        return EmploiTravailOccupation;
    }

    public void setEmploiTravailOccupation(String emploiTravailOccupation) {
        EmploiTravailOccupation = emploiTravailOccupation;
    }

    public Integer getNumTel() {
        return NumTel;
    }

    public void setNumTel(Integer numTel) {
        NumTel = numTel;
    }

    public int getBalance() {
        return Balance;
    }

    public void setBalance(int balance) {
        Balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User(String userName, String fullName, String lastName, String firstName, String adresse, String sexe, String password, String etatCivil, String religion, Date dateNaissance, String emploiTravailOccupation, Integer numTel, int balance) {
        UserName = userName;
        FullName = fullName;
        LastName = lastName;
        FirstName = firstName;
        Adresse = adresse;
        Sexe = sexe;
        Password = password;
        EtatCivil = etatCivil;
        Religion = religion;
        DateNaissance = dateNaissance;
        EmploiTravailOccupation = emploiTravailOccupation;
        NumTel = numTel;
        Balance = balance;
    }

    @Override
    public String toString() {
        return "User{" +
                "UserName='" + UserName + '\'' +
                ", FullName='" + FullName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", FirstName='" + FirstName + '\'' +
                ", Adresse='" + Adresse + '\'' +
                ", Sexe='" + Sexe + '\'' +
                ", Password='" + Password + '\'' +
                ", EtatCivil='" + EtatCivil + '\'' +
                ", Religion='" + Religion + '\'' +
                ", DateNaissance=" + DateNaissance +
                ", EmploiTravailOccupation='" + EmploiTravailOccupation + '\'' +
                ", NumTel=" + NumTel +
                ", Balance=" + Balance +
                '}';
    }

    public User(){}


}
