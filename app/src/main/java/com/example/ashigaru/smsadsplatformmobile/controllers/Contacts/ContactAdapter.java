package com.example.ashigaru.smsadsplatformmobile.controllers.Contacts;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseActivity;
import com.example.ashigaru.smsadsplatformmobile.Models.Contact;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;


import com.example.ashigaru.smsadsplatformmobile.controllers.TraiterContact.AddModifyContact;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.PendingIntent.getActivity;

/**

 */

public class ContactAdapter extends RecyclerSwipeAdapter<ContactAdapter.ViewHolder>  {

    List<Contact> listProposition;
    View rowView;
    private Context mContext;

    public ContactAdapter(Context mContext, List<Contact> listProposition) {
        this.listProposition = listProposition;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invitation, parent, false);
        ViewHolder viewHolder = new ViewHolder(rowView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        Contact proposition = listProposition.get(position);
        if(proposition.getFullName()==null) {
            viewHolder.swipeElememnt.setVisibility(View.GONE);
            viewHolder.linearLayoutAccept.setVisibility(View.GONE);
            viewHolder.linearLayoutRefuse.setVisibility(View.GONE);
        } else {
            viewHolder.swipeElememnt.setVisibility(View.VISIBLE);
            viewHolder.linearLayoutAccept.setVisibility(View.VISIBLE);
            viewHolder.linearLayoutRefuse.setVisibility(View.VISIBLE);
        }
        viewHolder.userName.setText(proposition.getFullName());
       // Date date = null;
       // SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
       // try {
        //    date = format.parse(proposition.getDate());
            //System.out.println(date);
        //} catch (ParseException e) {
        //    e.printStackTrace();
        //}
        //Calendar cal = Calendar.getInstance();
        //cal.setTime(date);
        //int year = cal.get(Calendar.YEAR);
       // int month = cal.get(Calendar.MONTH);
        //int day = cal.get(Calendar.DAY_OF_MONTH);
        //System.out.println(date);
        //System.out.println(year+"-"+month+"-"+day);;
        // viewHolder.dateInvit.setText(2000+"-"+11+"-"+11);
        viewHolder.subjectInvit.setText("Number: " +proposition.getNumTel());

       // viewHolder.timeInvit.setText(proposition.getTime());




        viewHolder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {

            @Override
            public void onOpen(SwipeLayout layout) {
                mItemManger.closeAllExcept(layout);
            }

        });

        mItemManger.bindView(viewHolder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return (null != listProposition ? listProposition.size() : 0);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeInvitation;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout linearLayoutRefuse, linearLayoutAccept,swipeElememnt;
        SwipeLayout swipeLayout;
        TextView timeInvit;
        TextView dateInvit;
        TextView subjectInvit;
        TextView userName;
        CircleImageView imgUser;

        public ViewHolder(View view) {
            super(view);
            this.swipeLayout =  view.findViewById(R.id.swipeInvitation);
            this.timeInvit =  view.findViewById(R.id.timeInvit);
            this.dateInvit = view.findViewById(R.id.dateInvit);
            this.subjectInvit =  view.findViewById(R.id.subjectInvit);
            this.userName =  view.findViewById(R.id.userName);
            this.imgUser =  view.findViewById(R.id.imgUser);
            this.swipeElememnt = view.findViewById(R.id.swipeElememnt);
            this.linearLayoutRefuse =  view.findViewById(R.id.linearLayoutRefuse);
            this.linearLayoutAccept =  view.findViewById(R.id.linearLayoutAccept);
            linearLayoutAccept.setOnClickListener(this);
            linearLayoutRefuse.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.linearLayoutRefuse: {

                    swipeLayout.close();
                    break;
                }
                case R.id.linearLayoutAccept: {

                    swipeLayout.close();
                    AddModifyContact AddModifyContact = new AddModifyContact();
                    Contact contactAndroid = new Contact();

                    contactAndroid.setNumTel(listProposition.get(getAdapterPosition()).getNumTel());
                    contactAndroid.setFullName(listProposition.get(getAdapterPosition()).getFullName());
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Contact",contactAndroid);
                    AddModifyContact.setArguments(bundle);
                    Log.v("selectContact",""+listProposition.get(getAdapterPosition()).getNumTel()+" "+listProposition.get(getAdapterPosition()).getFullName());
                    ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, AddModifyContact).commit();

                    break;
                }
            }
        }

        private void acceptInvitation(final Contact id) {
            final ProgressDialog progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("...");
            progressDialog.show();
            Log.v("addContact",""+Utils.urlServer +"/contacts/finddd/"+id.getNumTel());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.GET,
                    Utils.urlServer +"/contacts/finddd/"+6551651,
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            progressDialog.dismiss();
                            Log.v("ResponseServer" , response.toString());
                            Toast.makeText(mContext,"ajouter contact", Toast.LENGTH_LONG).show();
                            listProposition.remove(id);
                            notifyDataSetChanged();
                        }
                    },
                    new Response.ErrorListener(){
                        @Override
                        public void onErrorResponse(VolleyError error){
                            progressDialog.dismiss();
                            Toast.makeText(mContext,"Please Check your Internet Connection!" , Toast.LENGTH_SHORT).show();
                            Log.v("ResponseServer" , error.toString());

                        }
                    }
            );
            // Add JsonObjectRequest to the RequestQueue
            VolleySingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
        }

        private void refuseInvitation(final Contact id) {
            final ProgressDialog progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Accepting...");
            progressDialog.show();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.GET,
                    Utils.urlServer +"/propositionService/refuseProposition?idInvit=",
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            progressDialog.dismiss();
                            Toast.makeText(mContext,"supprimer contact", Toast.LENGTH_LONG).show();
                            listProposition.remove(id);
                            notifyDataSetChanged();
                        }
                    },
                    new Response.ErrorListener(){
                        @Override
                        public void onErrorResponse(VolleyError error){
                            progressDialog.dismiss();
                            Toast.makeText(mContext,"Please Check your Internet Connection!" , Toast.LENGTH_SHORT).show();
                            Log.v("ResponseServer" , error.toString());
                        }
                    }
            );
            // Add JsonObjectRequest to the RequestQueue
            VolleySingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
        }
    }



    public void updateData(final List<Contact> propositions ) {
        listProposition= new ArrayList<>();
        listProposition.addAll(propositions);
    }
}
