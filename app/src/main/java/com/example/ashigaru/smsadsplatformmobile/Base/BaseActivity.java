package com.example.ashigaru.smsadsplatformmobile.Base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.controllers.Guide.GuideActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;


public abstract class BaseActivity extends AppCompatActivity {

    protected IFragmentOnBackClick onBackPressedListener;
    private Unbinder unbinder;

    public abstract int getLayoutId();

    /**
     * Bind View
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        unbinder = ButterKnife.bind(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_guide, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_guide) {
            Toast.makeText(this, "Action clicked", Toast.LENGTH_LONG).show();
            finish();
            // startActivity(new Intent(this, HomeActivity.class));
            startActivity(new Intent(this, GuideActivity.class));
            return true;
    }return super.onOptionsItemSelected(item);
    }
    /**
     * Unbind View onDestroy
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }



    /**
     * to handle back button action in fragments
     *
     * @param onBackPressedListener
     */
    public void setOnBackPressedListener(IFragmentOnBackClick onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }
}
