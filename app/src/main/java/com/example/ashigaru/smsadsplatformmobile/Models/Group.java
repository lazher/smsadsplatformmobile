package com.example.ashigaru.smsadsplatformmobile.Models;

public class Group {
    private int id;
    private String titre;
    private int idUser;
    private String type;
    private int imgResource;
    private int colorRes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getImgResource() {
        return imgResource;
    }

    public void setImgResource(int imgResource) {
        this.imgResource = imgResource;
    }

    public int getColorRes() {
        return colorRes;
    }

    public void setColorRes(int colorRes) {
        this.colorRes = colorRes;
    }
    public Group(){}
    public Group(int id, String titre, int idUser, String type, int imgResource, int colorRes) {
        this.id = id;
        this.titre = titre;
        this.idUser = idUser;
        this.type = type;
        this.imgResource = imgResource;
        this.colorRes = colorRes;
    }
}