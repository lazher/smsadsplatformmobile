package com.example.ashigaru.smsadsplatformmobile.controllers.Dashboard;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseActivity;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseFragment;
import com.example.ashigaru.smsadsplatformmobile.Models.User;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;
import com.example.ashigaru.smsadsplatformmobile.controllers.HomeActivity;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;

import static android.content.Context.MODE_PRIVATE;

public class DashboardFragment extends BaseFragment {
    @BindView(R.id.userName)
    TextView userName;
    @BindView(R.id.nbHobbies)
    TextView nbHobbies ;
    @BindView(R.id.nbInvitation)
    TextView nbInvitation ;
    @BindView((R.id.nbActivity))
    TextView nbActivity;
    @Override
    public int getLayoutId() {
        return R.layout.activity_menu;
    }
    SharedPreferences sharedPreferences;
    private static final String SHARED_NAME = "user_prefs";
    User current;
    SharedPreferences shared;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return super.onCreateView(inflater,container,savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        super.setTitle("HOME");
        ((BaseActivity) getActivity()).setOnBackPressedListener(this);
        userName.setText("Welcome");

        shared =  getContext().getSharedPreferences(SHARED_NAME, MODE_PRIVATE);
        current = getCurrentUser();
        count();
        //countAdvertising();
        countInvit();
    }





    private void count() {


                String url = Utils.urlServer + "/contacts/count/"+current.getId();
                Log.v("url",""+url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("ResponseServer",""+response.toString());

                            nbHobbies.setText(""+response.toString());
                            Log.v("count", "" + response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        //Toast.makeText(getContext(),"Please Check your connection!" , Toast.LENGTH_SHORT).show();
                        Utils.showAlert(getActivity(), "Login", "Failed to login! verify your username or password");
                        Log.v("ResponseServer", error.toString());
                    }
                }
        );

        // Add JsonObjectRequest to the RequestQueue
        VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }


    private void countAdvertising() {


        String url = Utils.urlServer + "/pubs/count/"  ;
        Log.v("url",""+url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("ResponseServer", response.toString());
                        try {
                            nbActivity.setText(response.getString("count(*)"));
                            Log.v("count", "" + response.getString("count(*)"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        //Toast.makeText(getContext(),"Please Check your connection!" , Toast.LENGTH_SHORT).show();
                        Utils.showAlert(getActivity(), "Login", "Failed to login! verify your username or password");
                        Log.v("ResponseServer", error.toString());
                    }
                }
        );

        // Add JsonObjectRequest to the RequestQueue
        VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }
    private void countInvit() {


        String url = Utils.urlServer + "/contacts/countWithoutInvit/"+current.getId() ;
        Log.v("url",""+url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("ResponseServer", response.toString());
                        try {
                            nbInvitation.setText(response.getString("count(*)"));
                            Log.v("count", "" + response.getString("count(*)"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        //Toast.makeText(getContext(),"Please Check your connection!" , Toast.LENGTH_SHORT).show();
                        Utils.showAlert(getActivity(), "Login", "Failed to login! verify your username or password");
                        Log.v("ResponseServer", error.toString());
                    }
                }
        );

        // Add JsonObjectRequest to the RequestQueue
        VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }
    @Override
    public void onBackPressed() {

    }

    private User getCurrentUser() {
        Gson gson = new Gson();
        String jsonCurrent_user = shared.getString("current_user", "");
        User user = gson.fromJson(jsonCurrent_user, User.class);
        return  user;
    }
}
