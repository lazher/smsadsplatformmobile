package com.example.ashigaru.smsadsplatformmobile.Models;

import java.io.Serializable;
import java.util.Date;

public class Advertising implements Serializable {
    private int id;
    private String pubText;
    private Date date;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPubText() {
        return pubText;
    }

    public void setPubText(String pubText) {
        this.pubText = pubText;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    public Advertising(){}
    public Advertising(int id, String pubText, Date date , String url) {
        this.id = id;
        this.pubText = pubText;
        this.date = date;
        this.url =url;
    }
}
