package com.example.ashigaru.smsadsplatformmobile.controllers.TraiterContact;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseFragment;
import com.example.ashigaru.smsadsplatformmobile.Models.Contact;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;
import com.example.ashigaru.smsadsplatformmobile.controllers.Dashboard.DashboardFragment;
import com.kyleduo.switchbutton.SwitchButton;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnItemSelected;
import timber.log.Timber;

public class AddModifyContact extends BaseFragment implements View.OnClickListener{
    @BindView(R.id.FullName)
    EditText FullName;

    @BindView(R.id.Adresse)
    EditText Adresse;
    @BindView(R.id.NumTel)
    EditText NumTel;
    @BindView(R.id.Age)
    EditText Age;
    @BindView(R.id.Emploi)
    EditText Emploi;
     @BindView(R.id.sexeSwitch)
     SwitchButton sexeSwitch;
    @BindView(R.id.Religion)
    Spinner religion;
    @BindView(R.id.EtatCivil)
    Spinner etatCivil;
    @BindView(R.id.goAddButton)
    Button AddBtn;
    String sexeValue="";
    private ProgressDialog progress;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AddBtn.setOnClickListener(this);

        Bundle bundle = getArguments();
        Contact obj= (Contact) bundle.getSerializable("Contact");
        Log.v("obj from f2",obj.getFullName()+" "+obj.getNumTel());
        NumTel.setText(obj.getNumTel().toString());
        FullName.setText(obj.getFullName().toString());
        loadSpinnerIdTypes();
        sexeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sexeValue="male";
                } else {
                    sexeValue="female";
                }
            }
        });
    }


    @Override
    public int getLayoutId() {
        return R.layout.ajouter_modifier_contact_fragment;


    }
    @Override
    public void onBackPressed() {
        DashboardFragment dashboardFragment = new DashboardFragment();
        getFragmentManager().beginTransaction().replace(R.id.frame_container, dashboardFragment).commit();
    }

    private void loadSpinnerIdTypes() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.typr_religion , android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        religion.setAdapter(adapter);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(getContext(),
                R.array.typr_etatCivil , android.R.layout.simple_spinner_item);

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        etatCivil.setAdapter(adapter1);
    }
    @OnItemSelected(R.id.Religion)
    void onItemSelected(int position) {
        Timber.d("Element selected %s ", religion.getItemAtPosition(position));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.goAddButton : {
                addContact();
                break;
            }

        }
    }

    private void addContact() {

        Log.v("Clicked" , "clicked");
         final String FirstNameEdt = FullName.getText().toString();
        //String LastNameEdt = LastName.getText().toString();
       // String AdresseEdt = Adresse.getText().toString();
       // int AgeEdt=Integer.parseInt(Age.getText().toString());
        //int NumTelEdt =Integer.parseInt(NumTel.getText().toString());
       // String EmploiEdt= Emploi.getText().toString();
       // String sexeSwitchEdt= sexeSwitch.getText().toString();
       // String religionEdt= religion.getText().toString();
        //String etatCivilEdt = etatCivil.getText().toString();





        if(FullName.length() < 1) {
            Toast.makeText(getContext(),"FirstName should contains at least 1 caracters" , Toast.LENGTH_SHORT).show();
        }
           else {
                if (Adresse.length() < 1) {
                    Toast.makeText(getContext(), "Adresse should contains at least 1 caracters", Toast.LENGTH_SHORT).show();
                } else {
                    String url = Utils.urlServer + "/contacts/new";
                    //Utils.showProgressDialog(progress, "SmsAdsPlatform", "Adding Contact", false);

                    StringRequest rq = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                }
                            }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("Response Error", error.toString());
                            Toast.makeText(getContext(), "error", Toast.LENGTH_LONG).show();

                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<String, String>();
                            headers.put("Content-Type", "application/x-www-form-urlencoded");
                            return headers;
                        }

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("tag", "new");
                            params.put("Fullname", FirstNameEdt.toString());
                            Log.v("Fullname",""+FirstNameEdt.toString());

                            params.put("NumtelContact", NumTel.getText().toString());
                            params.put("Age", Age.getText().toString());
                            params.put("Emploi", Emploi.getText().toString());
                            params.put("Adresse", Adresse.getText().toString());
                            params.put("Sexe" ,sexeValue.toString());
                            params.put("id_user","1");
                            params.put("EtatCivil", etatCivil.getSelectedItem().toString());
                            params.put("Religion",religion.getSelectedItem().toString());
                            return params;
                        }

                    };


                    VolleySingleton.getInstance(getContext()).addToRequestQueue(rq);
                }

            }
        } ;
    }
