package com.example.ashigaru.smsadsplatformmobile.controllers.LoginSignUp;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.ashigaru.smsadsplatformmobile.Base.BaseFragment;
import com.example.ashigaru.smsadsplatformmobile.Models.User;
import com.example.ashigaru.smsadsplatformmobile.R;
import com.example.ashigaru.smsadsplatformmobile.Utils;
import com.example.ashigaru.smsadsplatformmobile.VolleySingleton;
import com.example.ashigaru.smsadsplatformmobile.controllers.Guide.GuideActivity;
import com.example.ashigaru.smsadsplatformmobile.controllers.HomeActivity;
import com.example.ashigaru.smsadsplatformmobile.controllers.LoginSignUp.SignUpFragment;
import com.google.gson.Gson;


import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseFragment implements View.OnClickListener   {


    @BindView(R.id.signupButton)
    Button SignUpBtn;
    @BindView(R.id.loginButton)
    Button loginBtn;
    @BindView(R.id.usernameLogin)
    EditText usernameEdt;
    @BindView(R.id.passwordLogin)
    EditText passwordEdt;

    private ProgressDialog progress;
    SharedPreferences sharedPreferences;



    private static final String SHARED_NAME = "user_prefs";

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_login;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        sharedPreferences = getActivity().getSharedPreferences(SHARED_NAME, MODE_PRIVATE);
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progress = new ProgressDialog(getContext());
        SignUpBtn.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
    }









    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.signupButton: {
                 getFragmentManager().beginTransaction().replace(R.id.logsign_container, new SignUpFragment()).addToBackStack("SIGNUP_FRAG").commit();
                break;
            }
            case R.id.loginButton: {
             authToServer();
               //startActivity(new Intent(getContext(), HomeActivity.class));
            }
        }
    }

    private void authToServer() {
        final User current_user = new User();
        final String username = usernameEdt.getText().toString().trim();
        final String pass = passwordEdt.getText().toString().trim();
        if(username.length()< 8) {
            Toast.makeText(getContext(),"Username should contains at least 8 caracters" , Toast.LENGTH_SHORT).show();
        } else {
            if (pass.length() < 8) {
                Toast.makeText(getContext(),"Password should contains at least 8 caracters" , Toast.LENGTH_SHORT).show();
            } else {
                Utils.showProgressDialog(progress, "SmsAdsPlatform Login", "Loading..", false);
                String url = Utils.urlServer + "/user_1/user/" + username+"/"+pass  ;
                Log.v("url",""+url);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                        Request.Method.GET,
                        url,
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.v("ResponseServer", response.toString());
                                try {
                                   // if ((response.getString("Username").equals(username)&&(response.getString("Password").equals(pass)))) {

                                    if ((response.getString("Username").equals(username)&&(response.getString("Password").equals(pass)))) {
                                        current_user.setUserName(response.getString("Username"));
                                        current_user.setFullName(response.getString("Fullname"));
                                        current_user.setId(response.getInt("id"));
                                        current_user.setPassword(response.getString("Password"));
                                        current_user.setAdresse(response.getString("Adresse"));
                                        current_user.setNumTel(response.getInt("Numtel"));
                                        // User.setBalance(response.getInt("Balance"));
                                        current_user.setEmail(response.getString("Email"));
                                        current_user.setReligion(response.getString("Religion"));
                                        current_user.setSexe(response.getString("Sexe"));
                                        current_user.setEmploiTravailOccupation(response.getString("Emploi"));



                                        // currentUser.setPictureUrl("https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Circle-icons-megaphone.svg/200px-Circle-icons-megaphone.svg.png");
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        Gson gson = new Gson();
                                        editor.putString("current_user", gson.toJson(current_user));
                                        editor.putString("Username", response.getString("Username"));
                                        editor.putString("Password", response.getString("Password"));
                                        editor.putString("Fullname", response.getString("Fullname"));

                                        editor.putString("Adresse", response.getString("Adresse"));
                                        editor.putString("Password", response.getString("Password"));



                                        editor.commit();


                                        Log.v("Saved !!", "Saved !!");
                                        progress.dismiss();
                                        Toast.makeText(getContext(),"Welcome  " + current_user.getFullName() , Toast.LENGTH_LONG  ).show();
                                        Log.v("Welcome","Welcome "+ current_user.getFullName());
                                      getActivity().finish();
                                        startActivity(new Intent(getContext(), GuideActivity.class));
                                    } else {
                                        progress.dismiss();
                                        Log.v("Login","Failed to login! verify your username or password");


                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progress.dismiss();
                                //Toast.makeText(getContext(),"Please Check your connection!" , Toast.LENGTH_SHORT).show();
                                Utils.showAlert(getActivity(), "Login", "Failed to login! verify your username or password");
                                Log.v("ResponseServer", error.toString());
                            }
                        }
                );

                // Add JsonObjectRequest to the RequestQueue
                VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
            }
        }

    }







    @Override
    public void onBackPressed() {
    }














}

