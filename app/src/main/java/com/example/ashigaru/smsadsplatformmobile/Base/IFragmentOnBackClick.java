package com.example.ashigaru.smsadsplatformmobile.Base;


/**
 * Handle Back Button Event for Fragment
 */
public interface IFragmentOnBackClick {
    void onBackPressed();
}
